package week4;
/***************
 Tiffany Quinlan
 Topic: HTTP/URL
 Date: 5/13/20
 **************/


import java.net.MalformedURLException;

import java.net.URL;
import java.util.*;

public class Main {

    public static void main(String[] args) throws MalformedURLException { // throw exception for URL not formatted correctly

        URL url = new URL("https://tiffquinlan.github.io/lesson3/");
        //URL Class methods
        System.out.println("Protocol: " + url.getProtocol());
        System.out.println("Host: " + url.getHost());
        System.out.println("Port: " + url.getPort());
        System.out.println("Path: " + url.getPath());
        // example from course this method gets the HTTP content for a website
        System.out.println(HTTPExample.getHttpContent("https://tiffquinlan.github.io/lesson3/"));

        // create a map and list using data returned from getHttpHeaders method
        Map<Integer, List<String>> m = HTTPExample.getHttpHeaders("https://tiffquinlan.github.io/lesson3/");

        // output the data in the map and list row by row
        for (Map.Entry<Integer,List<String>> entry : m.entrySet())
            try {
                System.out.println("Key= " + entry.getKey() + entry.getValue());
            } catch (Exception e) {
                System.err.println(e.toString());
            }

        // next step: Try to parse HTTP data as a JSON object
    }
}
